import {Routes, Route} from 'react-router-dom'
import HomeRoute from './components/HomeRoute'
import CartRoute from './components/CartRoute'
import FavRoute from './components/FavRoute'
import Counter from './components/Cart&FavCounter'
const Router = ({favourite, removeFromCart, items = [], FavouriteFn = () => {}, openModal = () => {}, addToCart = () => {}, cartNumber, favNumber, cart, openDeleteModal}) =>{
    return(
        <>
        <Counter cartNumber ={cartNumber} favNumber={favNumber}></Counter>
        <Routes>
            <Route path ='/' element = {<HomeRoute  items ={items} FavouriteFn ={FavouriteFn}  openModal ={openModal} addToCart = {addToCart} cartNumber ={cartNumber} favNumber = {favNumber} />}></Route>
            <Route path='/cart' element ={<CartRoute cart={cart} removeFromCart={removeFromCart} openDeleteModal={openDeleteModal}/>}></Route>
            <Route path ='/favourite' element ={<FavRoute openModal={openModal} favourite={favourite} FavouriteFn={FavouriteFn}></FavRoute>}></Route>
        </Routes>
        </>
    )
}

export default Router