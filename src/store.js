import { configureStore } from "@reduxjs/toolkit";
import storeSlice from "./components/redux/storeSlice";
import modalSlice from "./components/redux/modalSlice";

const store = configureStore({
    reducer : {
        store: storeSlice,
        modal: modalSlice
    }
})

export default store;