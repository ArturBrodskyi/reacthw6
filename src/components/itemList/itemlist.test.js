import ItemList from '.'
import {render} from '@testing-library/react'

describe("ItemList snapshot testing", () => {
    test("should ItemList render", () => {
      const { asFragment } = render(<ItemList></ItemList>);
  
      expect(asFragment()).toMatchSnapshot();
    });
  });
  