import { fireEvent, render, screen } from "@testing-library/react";
import Button from ".";


describe("button onClick test", () => {
  test("should onClick work", () => {
    const callback = jest.fn();
    render(<Button onClick={callback}>Hello</Button>);

    const btn = screen.getByText('Hello');

    fireEvent.click(btn)

    expect(callback).toBeCalled();
  });
});
