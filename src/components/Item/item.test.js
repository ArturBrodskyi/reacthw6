import Item from '.'
import {render} from '@testing-library/react'

describe("itemList snapshot testing", () => {
    test("should itemList render", () => {
      const { asFragment } = render(<Item></Item>);
  
      expect(asFragment()).toMatchSnapshot();
    });
  });
  