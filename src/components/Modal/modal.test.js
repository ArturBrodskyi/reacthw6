import Modal from '.'
import {render} from '@testing-library/react'

describe("modal snapshot testing", () => {
    test("should modal render", () => {
      const { asFragment } = render(<Modal></Modal>);
  
      expect(asFragment()).toMatchSnapshot();
    });
  });
  