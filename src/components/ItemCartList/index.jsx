import propTypes from "prop-types";
import ItemCart from "../ItemCart";

const ItemCartList = ({ items = [], removeFromCart, openDeleteModal }) => {
  return (
    <div className="itemContainer">
      {items && items.map((item) => item && <ItemCart item={item} key={item.articul} removeFromCart={removeFromCart} openDeleteModal={openDeleteModal} />)}
    </div>
  );
};

ItemCartList.propTypes = {
  items: propTypes.array,
};

export default ItemCartList;