import { Field, Formik, Form, ErrorMessage } from "formik";
import * as Yup from "yup";

const CheckoutModal = ({ isModalOpen, closeCheckoutModal }) => {
  return (
    <div style={{ display: isModalOpen ? "block" : "none" }}>
      <Formik
        initialValues={{
          firstName: "",
          lastName: "",
          age: "",
          address: "",
          mobileNumber: "",
        }}
        validationSchema={Yup.object({
          firstName: Yup.string()
            .max(15, "Must be 15 characters or less")
            .required("Name is required"),
          lastName: Yup.string()
            .max(20, "Must be 20 characters or less")
            .required("Required"),
          age: Yup.number()
            .min(14, "You must be older than 14")
            .max(100, "Enter correct age")
            .required("Age is required"),
          address: Yup.string()
            .max(100, "Must be 100 characters or less")
            .required("Address is required"),
          mobileNumber: Yup.string()
            .matches(/^[0-9]{10}$/, "Must be exactly 10 digits")
            .required("Mobile number is required"),
        })}
        onSubmit={(values) => {
          console.log(values);
          closeCheckoutModal()

        }}
      >
        <Form className="checkout">
          <div className="checkout-modal">
            <div className="name-input">
              <label htmlFor="firstName">First Name</label>
              <Field id="firstName" name="firstName" />
              <ErrorMessage name="firstName" component="div" className="error" />
            </div>

            <div className="lastName-input checkout-input">
              <label htmlFor="lastName">Last Name</label>
              <Field id="lastName" name="lastName" />
              <ErrorMessage name="lastName" component="div" className="error" />
            </div>

            <div className="age-input checkout-input">
              <label htmlFor="age">Age</label>
              <Field id="age" name="age" />
              <ErrorMessage name="age" component="div" className="error" />
            </div>

            <div className="address-input checkout-input">
              <label htmlFor="address">Address</label>
              <Field id="address" name="address" />
              <ErrorMessage name="address" component="div" className="error" />
            </div>

            <div className="mobile-input checkout-input">
              <label htmlFor="mobileNumber">Mobile Number</label>
              <Field id="mobileNumber" name="mobileNumber" />
              <ErrorMessage name="mobileNumber" component="div" className="error" />
            </div>
          </div>

          <button type="submit">Submit</button>
        </Form>
      </Formik>
    </div>
  );
};

export default CheckoutModal;
