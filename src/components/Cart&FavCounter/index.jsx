import Cart from "../svg/CartSvg";
import StarSvg from "../svg/StarSvg";

const Counter = ({cartNumber, favNumber}) => {
  return (
    <div className='counter'>
      <div className="fav">
        <StarSvg></StarSvg>
      </div>
      <p>{favNumber}</p>

      <div className="cart">
    <Cart></Cart>
      </div>
      <p>{cartNumber}</p>
    </div>
  );
};

export default Counter;
