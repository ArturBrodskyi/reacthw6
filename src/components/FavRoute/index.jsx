import ItemList from "../itemList";
import Counter from "../Cart&FavCounter";
import { useSelector } from "react-redux";
const FavRoute = ({FavouriteFn, openModal}) => {
  const favourites = useSelector(state => state.store.favourites)
  return (
    <>
      <ItemList
        items={favourites}
        FavouriteFn={FavouriteFn}
        openModal={openModal}
      ></ItemList>
    </>
  );
};

export default FavRoute;
