import reducer, {
  addInCart,
  deleteFromCart,
  favouriteFunction,
} from "./storeSlice";

describe("storeSlice reducers", () => {
  const initialState = {
    cart: [],
    items: [{ articul: "123", name: "Test Item", isFavourite: false }],
    favourites: [],
  };

  test("should add item to favourites", () => {
    const item = { articul: "123", name: "Test Item" };
    const action = favouriteFunction(item);
    const modifiedState = reducer(initialState, action);
    expect(modifiedState.favourites).toEqual([item]);
    expect(modifiedState.items[0].isFavourite).toBe(true);
  });

  test("should add item to cart", () => {
    const item = { articul: "123", name: "Test Item" };
    const action = addInCart(item);
    const modifiedState = reducer(initialState, action);
    expect(modifiedState.cart).toEqual([{ ...item, count: 1 }]);
  });

  test("should increment count if item exists in cart", () => {
    const item = { articul: "123", name: "Test Item" };
    const stateWithItem = { ...initialState, cart: [{ ...item, count: 1 }] };
    const action = addInCart(item);
    const modifiedState = reducer(stateWithItem, action);
    expect(modifiedState.cart).toEqual([{ ...item, count: 2 }]);
  });

  test("should delete item from cart", () => {
    const item = { articul: "123", name: "Test Item" };
    const stateWithItem = { ...initialState, cart: [{ ...item, count: 1 }] };
    const action = deleteFromCart("123");
    const modifiedState = reducer(stateWithItem, action);
    expect(modifiedState.cart).toEqual([]);
  });

  test("should add item to favourites", () => {
    const item = { articul: "123", name: "Test Item" };
    const action = favouriteFunction(item);
    const modifiedState = reducer(initialState, action);
    expect(modifiedState.favourites).toEqual([item]);
    expect(modifiedState.items[0].isFavourite).toBe(true);
  });

  test("should remove item from favourites", () => {
    const item = { articul: "123", name: "Test Item" };
    const stateWithFavourite = {
      ...initialState,
      favourites: [item],
      items: [{ ...item, isFavourite: true }],
    };
    const action = favouriteFunction(item);
    const modifiedState = reducer(stateWithFavourite, action);
    expect(modifiedState.favourites).toEqual([]);
    expect(modifiedState.items[0].isFavourite).toBe(false);
  });
});
