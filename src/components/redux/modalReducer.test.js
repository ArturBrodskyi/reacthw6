import modalReducer, {
  openModalSlice,
  closeModalSlice,
  openDeleteModalSlice,
  closeDeleteModalSlice,
  openCheckoutModal,
  closeCheckoutModal,
} from "./modalSlice";

describe("modalReducer", () => {
  const initialState = {
    isModalOpen: false,
    isDeleteModalOpen: false,
    isCheckoutModalOpen: false,
    selectedItem: '',
  };

  test("should return the initial state", () => {
    expect(modalReducer(undefined, {})).toEqual(initialState);
  });

  test("should handle openModalSlice", () => {
    const action = { type: openModalSlice.type, payload: 'item1' };
    const expectedState = { ...initialState, isModalOpen: true, selectedItem: 'item1' };
    expect(modalReducer(initialState, action)).toEqual(expectedState);
  });

  test("should handle closeModalSlice", () => {
    const action = { type: closeModalSlice.type, payload: '' };
    const modifiedState = { ...initialState, isModalOpen: true, selectedItem: 'item1' };
    const expectedState = { ...initialState, isModalOpen: false, selectedItem: '' };
    expect(modalReducer(modifiedState, action)).toEqual(expectedState);
  });

  test("should handle openDeleteModalSlice", () => {
    const action = { type: openDeleteModalSlice.type, payload: 'item2' };
    const expectedState = { ...initialState, isDeleteModalOpen: true, selectedItem: 'item2' };
    expect(modalReducer(initialState, action)).toEqual(expectedState);
  });

  test("should handle closeDeleteModalSlice", () => {
    const action = { type: closeDeleteModalSlice.type, payload: '' };
    const modifiedState = { ...initialState, isDeleteModalOpen: true, selectedItem: 'item2' };
    const expectedState = { ...initialState, isDeleteModalOpen: false, selectedItem: '' };
    expect(modalReducer(modifiedState, action)).toEqual(expectedState);
  });

  test("should handle openCheckoutModal", () => {
    const action = { type: openCheckoutModal.type };
    const expectedState = { ...initialState, isCheckoutModalOpen: true };
    expect(modalReducer(initialState, action)).toEqual(expectedState);
  });

  test("should handle closeCheckoutModal", () => {
    const action = { type: closeCheckoutModal.type };
    const modifiedState = { ...initialState, isCheckoutModalOpen: true };
    const expectedState = { ...initialState, isCheckoutModalOpen: false };
    expect(modalReducer(modifiedState, action)).toEqual(expectedState);
  });
});
