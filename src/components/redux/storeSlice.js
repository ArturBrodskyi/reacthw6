import { createAsyncThunk, createSlice } from "@reduxjs/toolkit"
import axios from 'axios';

const URL = "./arr.JSON";
const cartURL = './cartArr.JSON'

export const fetchItems = createAsyncThunk(
    'items/fetchItems',
    async () => {
        const response = await axios.get(URL);
        return response.data;
      }
)


const initialState = {
    cart:[],
    items:[],
    favourites:[]
}

const storeSlice = createSlice({
    name: 'store',
    initialState,
    reducers:{
        addInCart(state, action) {
            const item = action.payload;
            const itemExists = state.cart.find(cartItem => cartItem.articul === item.articul);

            if (itemExists) {
                itemExists.count++;
            } else {
                state.cart.push({ ...item, count: 1 });
            }
        },
        deleteFromCart(state, action){
            const itemId = action.payload;
            state.cart = state.cart.filter(item => item.articul !== itemId);
        },
        favouriteFunction(state, action) {
            const item = action.payload;
            const isFavourite = state.favourites.some(favItem => favItem.articul === item.articul);

            if (isFavourite) {
                state.favourites = state.favourites.filter(favItem => favItem.articul !== item.articul);
                state.items = state.items.map(i => 
                     i.articul === item.articul ? { ...i, isFavourite: false } : i
                );
            } else {
                state.favourites.push(item);
                state.items = state.items.map(i => 
                    i.articul === item.articul ? { ...i, isFavourite: true } : i
                );
            }
        }
        
        
    },
    extraReducers: (builder) => {
        builder.addCase(fetchItems.fulfilled, (state, action) => {
            state.items = action.payload;
        })
      },
})

export const {addInCart, deleteFromCart, favouriteFunction} = storeSlice.actions; 
export default storeSlice.reducer;