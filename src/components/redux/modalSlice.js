import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  isModalOpen: false,
  isDeleteModalOpen: false,
  isCheckoutModalOpen: false,
  selectedItem: '',
};

const modalSlice = createSlice({
  name: "modal",
  initialState,
  reducers: {
    openModalSlice(state, action) {
      state.isModalOpen = true;
      state.selectedItem = action.payload;
    },
    closeModalSlice(state, action) {
      state.isModalOpen = false;
      state.selectedItem = action.payload;
    },
    openDeleteModalSlice(state, action) {
      state.isDeleteModalOpen = true;
      state.selectedItem = action.payload;
    },
    closeDeleteModalSlice(state, action) {
      state.isDeleteModalOpen = false;
      state.selectedItem = action.payload;
    },
    openCheckoutModal(state) {
      state.isCheckoutModalOpen = true;
    },
    closeCheckoutModal(state) {
      state.isCheckoutModalOpen = false;
    },
  },
});

export const { openModalSlice, closeModalSlice, openDeleteModalSlice, closeDeleteModalSlice, openCheckoutModal, closeCheckoutModal } = modalSlice.actions;
export default modalSlice.reducer;
